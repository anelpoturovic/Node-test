const mongoose = require("mongoose");

// Connects to the database with the url from .env file
const connectApp = function () {
  return mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
};

module.exports = connectApp;
