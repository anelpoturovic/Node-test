const schedule = require('node-schedule');

// Creates a schedule that runs once a hour. 
const scheduler = (job) => {
  const rule = new schedule.RecurrenceRule();
  let minutes = new Date().getMinutes();
  if (minutes >= 58) {
    minutes = 1;
  }
  rule.minute = new Date().getMinutes() + 2;

  schedule.scheduleJob(rule, function () {
    job()
  });
};

module.exports = scheduler;
