var originalFetch = require('node-fetch');

//I am using fetch-retry to set retries in the code. 
var fetch = require('fetch-retry')(originalFetch, {
  retries: 2,
  retryDelay: 200,
});

const { addUpdatePosts } = require('../controller/posts-controller');

//Sets how many concurrent requests
const inc = 5;


// Function that fetches one post based on postID
let getPost = async (postId) => {
  try {
    let result = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${postId}
      `
    );

    result = await result.json();
    return result;
  } catch (error) {
    console.log(error.message);
  }
};

// Function that gets the posts 
const getPosts = async function () {
  try {
    for (let index = 0; index < 50; index += inc) {
      let postIds = Array.from(Array(index + inc + 1).keys());
      postIds = postIds.slice(postIds.length - inc, postIds.length);

      const posts = await Promise.all(
        postIds.map(async (id) => await getPost(id))
      );
      await addUpdatePosts(posts);
    }
  } catch (error) {
    console.log(error.message);
  }
};

module.exports = getPosts;
