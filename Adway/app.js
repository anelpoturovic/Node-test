
// Import diffrent packages
const express = require('express')
const dbConnection = require('./utils/db')
const scheduler = require('./utils/scheduler');
const getPosts = require('./utils/getPosts')

// Import routes 
const postRoutes = require('./routers/post-routes')

require('dotenv').config()

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.use('/posts',postRoutes)

const port = process.env.PORT || 3000

// Connects to the database 
dbConnection()
  .then(() => {
    app.listen(port, () => {
      console.log('server is up on port: ' + port);
      scheduler(getPosts);
    });
  })
  .catch((err) => {
    console.log(err);
  });


