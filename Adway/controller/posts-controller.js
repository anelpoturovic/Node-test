const Post = require('../models/post-model');

// Function to update the posts, 
const addUpdatePosts = async (posts) => {
  try {
    return await Promise.all(
      posts.map(async (post) => {
        const { id,title, body, userId } = post;
        let _post = await Post.findOne({ id })
        if(!_post){
            _post = new Post({id,title, body, userId })
            return await _post.save()
        }
        let modified = false
        if(_post.title != post.title) {
            _post.title = post.title
            modified = true
        }
        if(_post.body != post.body){
            _post.body = post.body
            modified = true
        }
        if(_post.userId != post.userId){
            _post.userId = post.userId
            modified = true
        }
        if(modified) return await _post.save()

        return
      })
    );
  } catch (error) {
    console.log(error.message);
  }
};

// Function to update the post in datatable
const updatePost = async (req, res, next) => {
  try {
    const { postId } = req.params;
    const { title, body } = req.body;
    const updateObj = {};
    if (title) updateObj.title = title;
    if (body) updateObj.body = body;
    const updatedPost = await Post.findByIdAndUpdate(postId, updateObj, {
      new: true,
    });
    res.send({ updatedPost });
  } catch (error) {

    res.status(500).send({ err: error.message });
  }
};

//exporting the functions to be able to use them later
module.exports = {
  addUpdatePosts,
  updatePost,
};
