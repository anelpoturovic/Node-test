//I am using express to setup the api to chage the post. 

const express = require('express')

const router = express.Router()

const {updatePost}  = require('../controller/posts-controller')

router.patch('/:postId',updatePost)

module.exports = router