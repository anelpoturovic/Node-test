const { Schema, model } = require('mongoose');

// Creates the schema for how the post is going to look in the database
const postSchema = Schema(
  {
    id: {
      type: Number,
      required: true,
      unique: true,
    },
    userId: {
      type: Number,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    body: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const Post = model('Post', postSchema);

module.exports = Post;
